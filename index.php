<!-- index.php -->


<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>s01- activity</title>
</head>
<body>

<!-- <h1>TEST</h1> -->
	
	<!-- ITEM 1 -->
	<div style="padding: 10px; border: 2px solid black; width: auto; display: inline-block;">
		
	<h2>Full Address</h2>

	<div>
		<?php 
		echo getFullAddress ("Philippines", "Metro Manila", "Quezon City", "3F Caswyn Bldg., Timog Ave.");
		?>
	</div>

	<div>
		<?php 
		echo getFullAddress ("Philippines", "Metro Manila", "Makati City", "3F Enzo Building");
		?>
	</div>

	</div>

	<div style="padding: 10px; border: none; width: auto; display: block;"></div>

	<!-- ITEM 2 -->
	<div style="padding: 10px; border: 2px solid black; width: auto; display: inline-block;">
		<h2>Letter-Based Grading</h2>
		<div>
			<?php 
			echo getLetterGrade(87);
			?>
		</div>
		<div>
			<?php 
			echo getLetterGrade(94);
			?>
		</div>
		<div>
			<?php 
			echo getLetterGrade(74);
			?>
		</div>
	</div>




</body>
</html>